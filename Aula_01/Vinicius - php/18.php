<?php
// Entrar via teclado com o valor de cinco produtos. Após as entradas, digitar um valor referente ao
// pagamento da somatória destes valores. Calcular e exibir o troco que deverá ser devolvido.


echo('Digite o valor dos 5 produtos: ');

$prod1 = trim(fgets(STDIN));
$prod2 = trim(fgets(STDIN));
$prod3 = trim(fgets(STDIN));
$prod4 = trim(fgets(STDIN));
$prod5 = trim(fgets(STDIN));


$valor = $prod1 + $prod2 + $prod3 + $prod4 + $prod5;

echo('Digite o pagamento: R$');
$pagamento = trim(fgets(STDIN));

$troco = $pagamento - $valor;

echo('O troco é de: R$'.$troco); 