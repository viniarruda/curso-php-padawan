<?php

// Calcular e exibir a área de um quadrado, a partir do valor de sua aresta que será digitado.

echo('Digite a aresta do quadrado: ');

$aresta = trim(fgets(STDIN));

$area = $aresta * $aresta;

echo('A área do quadrado é de: '.$area);