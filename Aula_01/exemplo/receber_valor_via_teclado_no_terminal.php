<?php

// Para receber um dado do usuário através do terminal você pode usar:
// Pra isso, basta executar o script com "php <nome do seu script php>";
// Toda vez que o interpretador chegar nessa linha ele vai esperar a digitação
// basta escrever o que quiser e teclar ENTER. Pronto, seu dado já foi recebido e atribuido a variavel, neste exemplo, $nome

$nome = trim(fgets(STDIN));